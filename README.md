# Ceros Ski Code Challenge

## Code Challenge Solution

### Changelog:

_(further details can be found below)_

- Fixed the bug where pressing the left key after player crashes breaks the game.
- Added tests to support same.
- Added a rhino non-playable character to chase the skier.
- Added a favicon, just for fun.
- Added restart button when Skier is devoured; game will effectively reset.
- Added a pause feature.
- Supplemented the partially deprecated use of the KeyboardEvent's "which" property with the recommended "key" property for handling player input.
- Added clarifying comments to new code.

## Details

### Left Button Bug Fix

In fixing the movement bug, I encountered a divergence from the bug report. In tests in both Chrome and FireFox (up to date versions), when the skier crashed, pressing up or right did nothing, pressing down moved the skier down through the obstacle, and pressing left crashed the game as noted in the instructions.

The right key did not stand the skier back up as indicated by the bug report. In fixing the bug, I chose to produce parity between the left and right keys, such that the left key no longer breaks the game, and neither left nor right keys will move the player once an obstacle is hit, and the player must use the down key.

### Player Jump

I implemented the player's jump using two of the jumping assets. In order to add a sense of challenge and realism, I chose to make the jump a little tricky, in that you need to be near the _top_ of the jump in order to clear the rock. The skier resource now tracks a `height` property that is compared against obstacle resources during collision.

### Rhino

A new rhino character extends the entity resource. After a short amount of time skiing, the rhino appears at the top of the screen and attempts to catch the skier. If the rhino encounters the skier, the skier is eaten and the game concludes. In order to add a measure of difficulty, the rhino runs faster than the skier skies, but slows more on diagonals, forcing the user to dodge in different directions in order to evade the rhino.

### Favicon

A favicon taken from the game's assets is dynamically added to the document during load. This is just to make things look nice.

### Game Restart

Once the player is caught by the Rhino, the game is over, and the user is presented with a dialogue providing the option to restart. Primary game processing is paused during the display of the dialogue. Pressing the restart button resets all pertinent game properties, but does not reinitialize the game resource itself, to save on processing.

### Game Pause

A player may pause or resume the game by pressing the "p" key. Both the pause function and the player's death leverage the same "paused" property on the game resource.

### Key Event Detection

The KeyboardEvent property `which` is in the process of being deprecated, but still sees wide use. It's therefore recommended to continue to support this option, but to supplement it with the more modern `key` property which will be implemented more widely in the future, once `which` is deprecated.

My implementation checks for the existence of the `key` property on the keyboard event, and if it is found, uses it. If it is not found, the `which` property is used instead.

### Unit Tests

Basic unit tests were added to check that the correct assets are loaded when a button is pressed after the skier crashes. Tests pass as written, and are confirmed to fail if the bug fix code is removed. Current unit tests leverage a `fakeCrash` method which simulates an obstacle collision, which can be applied to further tests.

---

Welcome to the Ceros Code Challenge - Ski Edition!

For this challenge, we have included some base code for Ceros Ski, our version of the classic Windows game SkiFree. If
you've never heard of SkiFree, Google has plenty of examples. Better yet, you can play our version here: 
http://ceros-ski.herokuapp.com/  

Or deploy it locally by running:
```
npm install
npm run dev
```

There is no exact time limit on this challenge and we understand that everyone has varying levels of free time. We'd 
rather you take the time and produce a solution up to your ability than rush and turn in a suboptimal challenge. Please 
look through the requirements below and let us know when you will have something for us to look at. If anything is 
unclear, don't hesitate to reach out.

**Requirements**

* **Fix a bug:**

  There is a bug in the game. Well, at least one bug that we know of. Use the following bug report to debug the code
  and fix it.
  * Steps to Reproduce:
    1. Load the game
    1. Crash into an obstacle
    1. Press the left arrow key
  * Expected Result: The skier gets up and is facing to the left
  * Actual Result: Giant blizzard occurs causing the screen to turn completely white (or maybe the game just crashes!)
  
* **Write unit tests:**

  The base code has Jest, a unit testing framework, installed. Write some unit tests to ensure that the above mentioned
  bug does not come back.
  
* **Extend existing functionality:**

  We want to see your ability to extend upon a part of the game that already exists. Add in the ability for the skier to 
  jump. The asset file for jumps is already included. All you gotta do is make the guy jump. We even included some jump 
  trick assets if you wanted to get really fancy!

  * Have the skier jump by either pressing a key or use the ramp asset to have the skier jump whenever he hits a ramp.
  * The skier should be able to jump over some obstacles while in the air. 
    * Rocks can be jumped over
    * Trees can NOT be jumped over
  * Anything else you'd like to add to the skier's jumping ability, go for it!

* **Build something new:**

  Now it's time to add something completely new. In the original Ski Free game, if you skied for too long, 
  a yeti would chase you down and eat you. In Ceros Ski, we've provided assets for a Rhino to run after the skier, 
  catch him and eat him.

  * The Rhino should appear after a set amount of time or distance skied and chase the skier, using the running assets
    we've provided to animate the rhino.
  * If the rhino catches the skier, it's game over and the rhino should eat the skier. 

* **Documentation:**

  * Update this README file with your comments about your work; what was done, what wasn't, features added & known bugs.
  * Provide a way for us to view the completed code and run it, either locally or through a cloud provider
  
* **Be original:**  

  * This should go without saying but don’t copy someone else’s game implementation!

**Grading** 

Your challenge will be graded based upon the following:

* How well you've followed the instructions. Did you do everything we said you should do?
* The quality of your code. We have a high standard for code quality and we expect all code to be up to production 
  quality before it gets to code review. Is it clean, maintainable, unit-testable, and scalable?
* The design of your solution and your ability to solve complex problems through simple and easy to read solutions.
* The effectiveness of your unit tests. Your tests should properly cover the code and methods being tested.
* How well you document your solution. We want to know what you did and why you did it.

**Bonus**

*Note: You won’t be marked down for excluding any of this, it’s purely bonus.  If you’re really up against the clock, 
make sure you complete all of the listed requirements and to focus on writing clean, well organized, and well documented 
code before taking on any of the bonus.*

If you're having fun with this, feel free to add more to it. Here's some ideas or come up with your own. We love seeing 
how creative candidates get with this.

* Provide a way to reset the game once it's over
* Provide a way to pause and resume the game
* Add a score that increments as the skier skis further
* Increase the difficulty the longer the skier skis (increase speed, increase obstacle frequency, etc.)
* Deploy the game to a server so that we can play it without having to install it locally
* Write more unit tests for your code

We are looking forward to see what you come up with!
