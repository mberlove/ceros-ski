export const GAME_WIDTH = window.innerWidth;
export const GAME_HEIGHT = window.innerHeight;

export const SKIER_CRASH = 'skierCrash';
export const SKIER_LEFT = 'skierLeft';
export const SKIER_LEFTDOWN = 'skierLeftDown';
export const SKIER_DOWN = 'skierDown';
export const SKIER_RIGHTDOWN = 'skierRightDown';
export const SKIER_RIGHT = 'skierRight';
export const SKIER_RISINGFALLING = 'skierRisingFalling';
export const SKIER_AIR = 'skierAir';

export const TREE = 'tree';
export const TREE_CLUSTER = 'treeCluster';
export const ROCK1 = 'rock1';
export const ROCK2 = 'rock2';

export const SKIER_STARTING_SPEED = 10;
export const RHINO_STARTING_SPEED = 11;
export const SKIER_DIAGONAL_SPEED_REDUCER = 1.4142;
export const RHINO_DIAGONAL_SPEED_REDUCER = 2;

export const RHINO_RUN1 = 'rhinoRun1';
export const RHINO_RUN2 = 'rhinoRun2';
export const RHINO_EAT1 = 'rhinoEat1';
export const RHINO_EAT2 = 'rhinoEat2';
export const RHINO_EAT3 = 'rhinoEat3';
export const RHINO_EAT4 = 'rhinoEat4';
export const RHINO_EAT5 = 'rhinoEat5';
export const RHINO_EAT6 = 'rhinoEat6';

export const RHINO_APPEAR_TIME = 100;
export const RHINO_MOVEMENT_BUFFER = 20;

export const ASSETS = {
    [SKIER_CRASH]: 'img/skier_crash.png',
    [SKIER_LEFT]: 'img/skier_left.png',
    [SKIER_LEFTDOWN]: 'img/skier_left_down.png',
    [SKIER_DOWN]: 'img/skier_down.png',
    [SKIER_RIGHTDOWN]: 'img/skier_right_down.png',
    [SKIER_RIGHT]: 'img/skier_right.png',
	[SKIER_RISINGFALLING]: 'img/skier_jump_1.png',
	[SKIER_AIR]: 'img/skier_jump_3.png',
    [TREE] : 'img/tree_1.png',
    [TREE_CLUSTER] : 'img/tree_cluster.png',
    [ROCK1] : 'img/rock_1.png',
    [ROCK2] : 'img/rock_2.png',
	[RHINO_RUN1]: 'img/rhino_run_left.png',
	[RHINO_RUN2]: 'img/rhino_run_left_2.png',
	[RHINO_EAT1]: 'img/rhino_lift.png',
	[RHINO_EAT2]: 'img/rhino_lift_mouth_open.png',
	[RHINO_EAT3]: 'img/rhino_lift_eat_1.png',
	[RHINO_EAT4]: 'img/rhino_lift_eat_2.png',
	[RHINO_EAT5]: 'img/rhino_lift_eat_3.png',
	[RHINO_EAT6]: 'img/rhino_lift_eat_4.png'
};

export const SKIER_DIRECTIONS = {
    CRASH : 0,
    LEFT : 1,
    LEFT_DOWN : 2,
    DOWN : 3,
    RIGHT_DOWN : 4,
    RIGHT : 5
};

export const RHINO_DIRECTIONS = {
    STATIC : 0,
    LEFT : 1,
    LEFT_DOWN : 2,
    DOWN : 3,
    RIGHT_DOWN : 4,
    RIGHT : 5,
	RIGHT_UP : 6,
	UP : 7,
	LEFT_UP : 8
};

export const SKIER_DIRECTION_ASSET = {
    [SKIER_DIRECTIONS.CRASH] : SKIER_CRASH,
    [SKIER_DIRECTIONS.LEFT] : SKIER_LEFT,
    [SKIER_DIRECTIONS.LEFT_DOWN] : SKIER_LEFTDOWN,
    [SKIER_DIRECTIONS.DOWN] : SKIER_DOWN,
    [SKIER_DIRECTIONS.RIGHT_DOWN] : SKIER_RIGHTDOWN,
    [SKIER_DIRECTIONS.RIGHT] : SKIER_RIGHT
};

export const SKIER_HEIGHTS = {
	GROUND: 0,
	RISINGFALLING: 1,
	AIR: 2,
	ABOUT_TO_FALL: 3
};

export const SKIER_JUMP_STATES = {
	GROUND: 0,
	RISING: 1,
	FALLING: 2
};

export const SKIER_HEIGHTS_ASSET = {
	[SKIER_HEIGHTS.RISINGFALLING]: SKIER_RISINGFALLING,
	[SKIER_HEIGHTS.AIR]: SKIER_AIR,
	[SKIER_HEIGHTS.ABOUT_TO_FALL]: SKIER_AIR
};

export const RHINO_RUN_ASSET = [
	RHINO_RUN1, 
	RHINO_RUN2
];

export const RHINO_EAT_ASSET = [
	RHINO_EAT1,
	RHINO_EAT2,
	RHINO_EAT3,
	RHINO_EAT4,
	RHINO_EAT5,
	RHINO_EAT6
];

export const KEYS = {
    LEFT : [37, "ArrowLeft"],
    RIGHT : [39, "ArrowRight"],
    UP : [38, "ArrowUp"],
    DOWN : [40, "ArrowDown"],
	SPACE: [32, " "],
	P: [112, "p"]
};