import * as Constants from "../Constants";
import { AssetManager } from "./AssetManager";
import { Canvas } from './Canvas';
import { Skier } from "../Entities/Skier";
import { Rhino } from "../Entities/Rhino";
import { ObstacleManager } from "../Entities/Obstacles/ObstacleManager";
import { Rect } from './Utils';

export class Game {
	
    gameWindow = null;
	gameTicks = 0;
	rhino = false;
	paused = false;

    constructor() {
        this.assetManager = new AssetManager();
        this.canvas = new Canvas(Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
        this.skier = new Skier(0, 0);
        this.obstacleManager = new ObstacleManager();

        document.addEventListener('keydown', this.handleKeyDown.bind(this));
    }

    init() {
        this.obstacleManager.placeInitialObstacles();
    }

    async load() {
        await this.assetManager.loadAssets(Constants.ASSETS);
    }

    run() {
		
		// Don't process main events if game is not running.
		if (!this.paused) {
		
			this.canvas.clearCanvas();

			this.updateGameWindow();
			this.drawGameWindow();
		
		}

        requestAnimationFrame(this.run.bind(this));
		
		// Track game time only if game is running.
		if (!this.paused) { this.gameTicks += 1 }
		
    }
	
	// Present a restart dialogue to the user, and respond to it.
	playAgain() {
		
		let prompt = document.createElement("div");
		prompt.id = "restartPrompt";
		
		let banner = prompt.appendChild(document.createElement("div"));
		banner.innerText = "Would you like to play again?";
		
		let button = document.createElement("button");
		banner.appendChild(button);
		
		button.innerText="Restart";
		
		// If the button is clicked, reset the game state.
		button.addEventListener('click', e => {
			this.skier.reset(0, 0);
			this.gameTicks = 0;
			this.rhino = false;
			this.paused = false;
			e.target.parentNode.parentNode.remove();
		});
		
		document.body.appendChild(prompt);
		
	}

    updateGameWindow() {
				
		this.skier.move();

        const previousGameWindow = this.gameWindow;
        this.calculateGameWindow();

        this.obstacleManager.placeNewObstacle(this.gameWindow, previousGameWindow);

        this.skier.checkIfSkierHitObstacle(this.obstacleManager, this.assetManager);
		
		// If there's a rhino, move it, and check if it got the player.
		if (this.rhino) { 
			this.rhino.move();
			if (this.rhino.checkIfHitSkier(this.assetManager)) {
				this.paused = true;
				this.playAgain();
			}
		}
		
		// Add the rhino if it has been long enough.
		else if (this.gameTicks > Constants.RHINO_APPEAR_TIME) {
			this.rhino = new Rhino(0,0,this.skier);
		}

    }

    drawGameWindow() {
        this.canvas.setDrawOffset(this.gameWindow.left, this.gameWindow.top);

        this.skier.draw(this.canvas, this.assetManager); 
		if (this.rhino) { this.rhino.draw(this.canvas, this.assetManager); }
		
        this.obstacleManager.drawObstacles(this.canvas, this.assetManager);
    }

    calculateGameWindow() {
        const skierPosition = this.skier.getPosition();
        const left = skierPosition.x - (Constants.GAME_WIDTH / 2);
        const top = skierPosition.y - (Constants.GAME_HEIGHT / 2);

        this.gameWindow = new Rect(left, top, left + Constants.GAME_WIDTH, top + Constants.GAME_HEIGHT);
    }

    handleKeyDown(event) {
		
		// Use the "key" property if available; otherwise fall back to "which"
		let keyboardEventType = Number("key" in event);
		let keyboardEvent = (keyboardEventType ? event.key : event.which);
		
        switch(keyboardEvent) {
            case Constants.KEYS.LEFT[keyboardEventType]:
                this.skier.turnLeft();
                event.preventDefault();
                break;
            case Constants.KEYS.RIGHT[keyboardEventType]:
                this.skier.turnRight();
                event.preventDefault();
                break;
            case Constants.KEYS.UP[keyboardEventType]:
                this.skier.turnUp();
                event.preventDefault();
                break;
            case Constants.KEYS.DOWN[keyboardEventType]:
                this.skier.turnDown();
                event.preventDefault();
                break;
			case Constants.KEYS.SPACE[keyboardEventType]:
				this.skier.jump();
				event.preventDefault();
				break;
			case Constants.KEYS.P[keyboardEventType]:
				this.paused = !this.paused;
				break;
        }
    }
}