import "babel-polyfill";
import { Skier } from "../Entities/Skier";
import { ObstacleManager } from "../Entities/Obstacles/ObstacleManager";
import { AssetManager } from "./AssetManager";
import * as Constants from "../Constants";

let s = new Skier(0,0);
let o = new ObstacleManager();
let a = new AssetManager();

function fakeCrash() {
	s = new Skier(0,0);
	a.loadAssets(Constants.ASSETS).then(() => {
		let p = s.getPosition();
		o.placeRandomObstacle(p.x, p.y, p.x, p.y);
		return s.checkIfSkierHitObstacle(o, a);
	});
}

test('left key recovers from a crash',  () => {
	let hit = fakeCrash();
	s.turnLeft();
	let asset = s.getAsset();
	expect(asset).toBe(Constants.SKIER_DIRECTION_ASSET[Constants.SKIER_DIRECTIONS.LEFT_DOWN]);
});

test('right key recovers from a crash',  () => {
	let hit = fakeCrash();		
	s.turnRight();
	let asset = s.getAsset();
	expect(asset).toBe(Constants.SKIER_DIRECTION_ASSET[Constants.SKIER_DIRECTIONS.RIGHT_DOWN]);
});