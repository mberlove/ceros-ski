import * as Constants from "../../Constants";
import { Entity } from "../Entity";
import { randomInt } from '../../Core/Utils';

const assetTypes = [
    Constants.TREE,
    Constants.TREE_CLUSTER,
    Constants.ROCK1,
    Constants.ROCK2
];

// Track obstacle heights to allow jump collision detection.
const assetHeights = {
	[Constants.TREE]: Constants.SKIER_HEIGHTS.AIR, /* Guarantee that tree will always crash */
	[Constants.TREE_CLUSTER]: Constants.SKIER_HEIGHTS.AIR,
	[Constants.ROCK1]: Constants.SKIER_HEIGHTS.AIR - 1, /* Flexible depending on # of skier height states */
	[Constants.ROCK2]: Constants.SKIER_HEIGHTS.AIR - 1
};

export class Obstacle extends Entity {
    constructor(x, y) {
        super(x, y);

        const assetIdx = randomInt(0, assetTypes.length - 1);
        this.assetName = assetTypes[assetIdx];
		this.height = assetHeights[this.assetName];
    }
}