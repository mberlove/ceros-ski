import * as Constants from "../Constants";
import { Entity } from "./Entity";
import { intersectTwoRects, Rect } from "../Core/Utils";

export class Rhino extends Entity {
    assetName = Constants.RHINO_RUN1;

    speed = Constants.RHINO_STARTING_SPEED;
	
	// For tracking asset and state.
	eating = -1;
	running = 0;

    constructor(x, y, skier) {
        super(x, y);
		this.skier = skier;
    }
	
	// Leverage the appropriate asset based on context.
    updateAsset() {
		if (this.eating > 0) {
			this.assetName = Constants.RHINO_EAT_ASSET[this.eating];
		}
		else {
			this.assetName = Constants.RHINO_RUN_ASSET[Math.round(this.running)];
		}
    }

	// Switch from running to eating states.
	startEating() {
		this.running = -1;
		this.eating = 0;
	}
	
	// Find the direction of the skier (used to simplify movement controls).
	trackSkier(skier_x, skier_y) {
		
		let direction = Constants.RHINO_DIRECTIONS.DOWN;
		
		if (skier_y < this.y) { direction = Constants.RHINO_DIRECTIONS.UP }
		
		if (skier_x > this.x + Constants.RHINO_MOVEMENT_BUFFER) {
			direction += (direction == Constants.RHINO_DIRECTIONS.DOWN ? 1 : -1);
		}
		else if (skier_x < this.x - Constants.RHINO_MOVEMENT_BUFFER) {
			direction -= (direction == Constants.RHINO_DIRECTIONS.DOWN ? 1 : -1);
		}
		
		return direction;
		
	}
			
    move() {
		
		if (this.eating == -1) {
			
			let skier_position = this.skier.getPosition();
			
			this.running += 0.1;
			
			// Allow for additional running sprites if desired.
			if (this.running > Constants.RHINO_RUN_ASSET.length-1) {
				this.running = 0;
			}
			
			// Track the skier and move in that direction.
			let nextMove = this.trackSkier(skier_position.x, skier_position.y);
			
			switch(nextMove) {
				case Constants.RHINO_DIRECTIONS.LEFT_DOWN:
					this.moveRhinoLeftDown();
					break;
				case Constants.RHINO_DIRECTIONS.DOWN:
					this.moveRhinoDown();
					break;
				case Constants.RHINO_DIRECTIONS.RIGHT_DOWN:
					this.moveRhinoRightDown();
					break;
				case Constants.RHINO_DIRECTIONS.LEFT_UP:
					this.moveRhinoLeftUp();
					break;
				case Constants.RHINO_DIRECTIONS.UP:
					this.moveRhinoUp();
					break;
				case Constants.RHINO_DIRECTIONS.RIGHT_UP:
					this.moveRhinoRightUp();
					break;
			}
			
		}
		
		// If the rhino has caught the skier, eat.
		else {
			
			if (this.eating == Constants.RHINO_EAT_ASSET.length-1) {
				this.eating = -1;
				this.running = 0;
				return;
			}
			this.eating += 1;
			
		}
		
		this.updateAsset();
		
    }

    moveRhinoLeftDown() {
        this.x -= this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
    }
	
	moveRhinoLeftUp() {
        this.x -= this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
        this.y -= this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
    }

    moveRhinoDown() {
        this.y += this.speed;
    }

    moveRhinoRightDown() {
        this.x += this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
    }
	
	moveRhinoRightUp() {
        this.x += this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
        this.y -= this.speed / Constants.RHINO_DIAGONAL_SPEED_REDUCER;
    }
	
    moveRhinoUp() {
        this.y -= Constants.RHINO_STARTING_SPEED;
    }

	// If the rhino has caught the skier, toggle between running and eating states.
    checkIfHitSkier(assetManager) {
		const skier_asset = assetManager.getAsset(this.skier.getAsset());
		const skier_position = this.skier.getPosition();
        const skierBounds = new Rect(
            skier_position.x - skier_asset.width / 2,
            skier_position.y - skier_asset.height / 2,
            skier_position.x + skier_asset.width / 2,
            skier_position.y - skier_asset.height / 4
        );
		
		const asset = assetManager.getAsset(this.assetName);
		const rhinoBounds = new Rect(
			this.x - asset.width / 2,
			this.y - asset.height / 2,
			this.x + asset.width / 2,
			this.y
		);
		
		let caught = intersectTwoRects(skierBounds, rhinoBounds);
		if (caught) { this.startEating(); }
		return caught;	
    }
}