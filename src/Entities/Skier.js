import * as Constants from "../Constants";
import { Entity } from "./Entity";
import { intersectTwoRects, Rect } from "../Core/Utils";

export class Skier extends Entity {
    assetName = Constants.SKIER_DOWN;

    direction = Constants.SKIER_DIRECTIONS.DOWN;
    speed = Constants.SKIER_STARTING_SPEED;
	height = Constants.SKIER_HEIGHTS.GROUND;
	jumping = Constants.SKIER_JUMP_STATES.GROUND;

    constructor(x, y) {
        super(x, y);
    }

    setDirection(direction) {
        this.direction = direction;
        this.updateAsset();
    }
	
	setHeight(height) {
		this.height = height;
		this.updateAsset();
	}
	
	getAsset() {
		return this.assetName;
	}
	
	reset(x, y) {
		this.x = x;
		this.y = y;
		this.direction = Constants.SKIER_DIRECTIONS.DOWN;
		this.assetName = Constants.SKIER_DIRECTION_ASSET[this.direction];
		this.speed = Constants.SKIER_STARTING_SPEED;
		this.height = Constants.SKIER_HEIGHTS.GROUND;
		this.jumping = Constants.SKIER_JUMP_STATES.GROUND;
	}

    updateAsset() {
		// Skier can have a conceptual direction and height at the same time, 
		// but only one visual asset at a time.
		// Jumping assets should take precedence over ground directional assets
		// (i.e jumping is monodirectional)
		if (this.height > Constants.SKIER_HEIGHTS.GROUND) {
			this.assetName = Constants.SKIER_HEIGHTS_ASSET[Math.abs(Math.ceil(this.height))];
		}
		else {
			this.assetName = Constants.SKIER_DIRECTION_ASSET[this.direction];
		}
    }

    move() {
        switch(this.direction) {
            case Constants.SKIER_DIRECTIONS.LEFT_DOWN:
                this.moveSkierLeftDown();
                break;
            case Constants.SKIER_DIRECTIONS.DOWN:
                this.moveSkierDown();
                break;
            case Constants.SKIER_DIRECTIONS.RIGHT_DOWN:
                this.moveSkierRightDown();
                break;
        }
		if (this.jumping) {
			this.moveSkierThroughJump();
		}
    }

    moveSkierLeft() {
        this.x -= Constants.SKIER_STARTING_SPEED;
    }

    moveSkierLeftDown() {
        this.x -= this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
    }

    moveSkierDown() {
        this.y += this.speed;
    }

    moveSkierRightDown() {
        this.x += this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
    }

    moveSkierRight() {
        this.x += Constants.SKIER_STARTING_SPEED;
    }

    moveSkierUp() {
        this.y -= Constants.SKIER_STARTING_SPEED;
    }
		
	// Iterate through the skier's jump states.
	moveSkierThroughJump() {
	
		if (this.jumping === Constants.SKIER_JUMP_STATES.RISING) {
			this.setHeight(this.height+0.05);
		}
		
		else if (this.jumping === Constants.SKIER_JUMP_STATES.FALLING) {
			this.setHeight(this.height-0.05); 
		}
		
		if (this.height >= Constants.SKIER_HEIGHTS.AIR) {
			this.jumping = Constants.SKIER_JUMP_STATES.FALLING;
		}
		
		else if (this.height <= Constants.SKIER_HEIGHTS.GROUND) {
			this.jumping = Constants.SKIER_JUMP_STATES.GROUND;
			this.setHeight(Constants.SKIER_HEIGHTS.GROUND);
		}
		
	}

    turnLeft() {
        if(this.direction === Constants.SKIER_DIRECTIONS.LEFT) {
            this.moveSkierLeft();
        }
		else if (this.direction === Constants.SKIER_DIRECTIONS.CRASH) {
			this.setDirection(Constants.SKIER_DIRECTIONS.LEFT);
		}
        else {
            this.setDirection(this.direction - 1);
        }
    }

    turnRight() {
        if(this.direction === Constants.SKIER_DIRECTIONS.RIGHT) {
            this.moveSkierRight();
        }
        else {
            this.setDirection(this.direction + 1);
        }
    }

    turnUp() {
        if(this.direction === Constants.SKIER_DIRECTIONS.LEFT || this.direction === Constants.SKIER_DIRECTIONS.RIGHT) {
            this.moveSkierUp();
        }
    }

    turnDown() {
        this.setDirection(Constants.SKIER_DIRECTIONS.DOWN);
    }
	
	// Indicate that the skier is now jumping, if not already in the air.
	jump() {
		if (this.height === Constants.SKIER_HEIGHTS.GROUND) { 
			this.jumping = Constants.SKIER_JUMP_STATES.RISING
		}
	}

    checkIfSkierHitObstacle(obstacleManager, assetManager) {
		const asset = assetManager.getAsset(this.assetName);
        const skierBounds = new Rect(
            this.x - asset.width / 2,
            this.y - asset.height / 2,
            this.x + asset.width / 2,
            this.y - asset.height / 4
        );

        const collision = obstacleManager.getObstacles().find((obstacle) => {
            const obstacleAsset = assetManager.getAsset(obstacle.getAssetName());
            const obstaclePosition = obstacle.getPosition();
			const obstacleHeight = obstacle.getHeight(); // For comparing heights.
            const obstacleBounds = new Rect(
                obstaclePosition.x - obstacleAsset.width / 2,
                obstaclePosition.y - obstacleAsset.height / 2,
                obstaclePosition.x + obstacleAsset.width / 2,
                obstaclePosition.y
            );

            return intersectTwoRects(skierBounds, obstacleBounds) && obstacleHeight >= this.height;
        });

        if(collision) {
            this.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
        }
    };
}