import '../css/game.css';
import { Game } from './Core/Game.js';

document.head || (document.head = document.getElementsByTagName('head')[0]);

// Add favicon.
var link = document.createElement('link');
link.id = 'favicon';
link.rel = 'shortcut icon';
link.href = '../img/skier_jump_1.png';
document.head.appendChild(link);

document.addEventListener("DOMContentLoaded",() => {
    const skiGame = new Game();
    skiGame.load().then(() => {
        skiGame.init();
        skiGame.run();
    });
});